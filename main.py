import json
from pathlib import Path
from sly import Lexer, Parser

'''
directives ::= directive directives | empty
directive ::= simple directive | block directive
names ::= name names | empty
simple directive ::= name names ";"
block_directive ::= name names "{" directives "}"
'''


class ConfLexer(Lexer):
    tokens = {BEGIN, END, NAME, SEMICOLON, STRING}

    # Special symbols
    BEGIN = r'{'
    END = r'}'
    SEMICOLON = r';'

    # Ignored pattern
    ignore = r', \t'
    ignore_comment = r'\#.*'
    ignore_newline = r'\n+'

    # Tokens
    NAME = r'[^,\t\#{};\']+'
    STRING = r"'.*'"


class ConfParser(Parser):
    tokens = ConfLexer.tokens

    @_('directive directives')
    def directives(self, p):
        return [p.directive] + p.directives

    @_('empty')
    def directives(self, p):
        return []

    @_('simple_directive', 'block_directive')
    def directive(self, p):
        return p[0]

    def get_item(self, name, dict_1):
        key = name.split(':')[0]
        value = str(name.split(':')[1]).strip()
        try:
            value = int(value)
        except Exception:
            pass
        dict_1[key] = value
        return dict_1

    @_('name names SEMICOLON')
    def simple_directive(self, p):
        dict_1 = self.get_item(p.name, {})
        for i in p.names:
            dict_1 = self.get_item(i, dict_1)
        return dict_1

    @_('name names BEGIN directives END')
    def block_directive(self, p):
        return p.directives[0]

    @_('name names')
    def names(self, p):
        return [p.name] + p.names

    @_('empty')
    def names(self, p):
        return []

    @_('NAME', 'STRING')
    def name(self, p):
        return p[0].replace('"', '')

    @_('')
    def empty(self, p):
        pass


try:
    text = Path(input()).read_text()
    lexer = ConfLexer()
    parser = ConfParser()
    data = parser.parse(lexer.tokenize(text))
    result_dict = dict(students=data)
    Path("result.json").write_text(json.dumps(result_dict, indent=2, ensure_ascii=False))
except:
    print("Invalid file")
